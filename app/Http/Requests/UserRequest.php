<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'email' => 'required|min:4|email',
            'address' => 'required|max:150',
            'password' => 'required|min:6',
            'password_confirmation' => 'required'
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'You have to specify your name',
            'name.max' => 'Your name cant be more than 100 char',
            'email.required' => 'You have to specify your email',
            'email.min' => 'Your email must be more than 4 char',
            'email.email' => 'Be careful this is not an email',
            'password.required' => 'You must put specify a password',
            'password.min' => 'Your password must be more than 6 char'
        ];
    }
}
